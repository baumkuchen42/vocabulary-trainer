var word_count = 0;
var enter_exercise_mode = function (shuffled) {
  if(localStorage.length != 0){
    var query_word;
    if(shuffled){
      query_word = select_random_word();
    }
    else{
      query_word = select_next_word();
      word_count++;
    }
    document.querySelector('#query_word').innerHTML = query_word;
    var answer_word_input = document.querySelector('#answer_word_input');
    answer_word_input.focus();
    answer_word_input.onkeydown = function (event) {
      if(event.key === 'Enter'){
         evaluate_answer(query_word, answer_word_input.value);
      }
    }
  }
  else{
    no_vocabulary_found();
  }
}

var no_vocabulary_found = function () {
  var no_vocabulary_page = document.createElement('html');
  no_vocabulary_page.innerHTML = load_html('sub/no_vocabulary.html');
  var no_vocabulary_card = no_vocabulary_page.querySelector('.no_vocabulary_card');
  outer.replaceChild(no_vocabulary_card, outer.childNodes[2]);
  var goto_add_btn = document.querySelector('#goto_add_btn');
  goto_add_btn.onclick = start_add;
}

var evaluate_answer = function (query, answer) {
  correct_answer = localStorage.getItem(query)
  if( answer == correct_answer){
    show_correct_page();
  }
  else if (answer == '') {
    trigger_empty_warning();
  }
  else{
    show_wrong_page(correct_answer);
  }
}

var show_correct_page = function () {
  var correct_page = document.createElement('html');
  correct_page.innerHTML = load_html('sub/correct.html');
  var correct_card = correct_page.querySelector('.correct_card');
  outer.replaceChild(correct_card, outer.childNodes[2]);
  document.querySelector('.continue_btn').onclick = start_exercise;
  correct_card.focus();
  correct_card.onkeydown = function (event) {
    if(event.key == 'Enter'){
      start_exercise();
    }
  }
}

var show_wrong_page = function (correct_answer) {
  var wrong_page = document.createElement('html');
  wrong_page.innerHTML = load_html('sub/wrong.html');
  var wrong_card = wrong_page.querySelector('.wrong_card');
  outer.replaceChild(wrong_card, outer.childNodes[2]);
  document.querySelector('#correct_answer').innerHTML = correct_answer;
  document.querySelector('.continue_btn').onclick = start_exercise;
  wrong_card.focus();
  wrong_card.onkeydown = function (event) {
    if(event.key == 'Enter'){
      start_exercise();
    }
  }
}

var select_random_word = function () {
  return random_query_word = localStorage.key(Math.floor((Math.random() * localStorage.length-1) + 1));
}

var select_next_word = function () {
  if(word_count < localStorage.length){
    return localStorage.key(word_count);
  }
  else {
    word_count = 0;
    return localStorage.key(word_count);
  }
}
